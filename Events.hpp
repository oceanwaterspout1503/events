//
//  Events.hpp
//  лаба1
//
//  Created by Maria Khaleta on 14.12.17.
//  Copyright © 2017 Maria Khaleta. All rights reserved.
//

#ifndef Events_hpp
#define Events_hpp
#include <iostream>
#include <stdio.h>
#include <string>
#include "Data.hpp"
#include "Price.hpp"
#include "Place.hpp"
//#include "Fectival.hpp"

//#include "Place"
class Events {
private:
    std::string title;
    std::string artist; // имя артиста или групы
    Data data;
    Place place;
    std:: string  type_events;
    
public:
    Events();
    ~Events();
    void add_Date(); //установка даты и времени проведения
    void add_Place();
    virtual void Type() = 0;
    void add_Artist();
    void add_Title();

};
#endif /* Events_hpp */
