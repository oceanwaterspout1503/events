//
//  Concert.hpp
//  лаба1
//
//  Created by Maria Khaleta on 14.12.17.
//  Copyright © 2017 Maria Khaleta. All rights reserved.
//

#ifndef Concert_hpp
#define Concert_hpp
#include "Events.hpp"
#include <stdio.h>
class Concert : public Events {//наследуется от ивента
private:
    std::string type_of_event = "Concert";
    std::string type_of_concert;
public:
    Concert();
    ~Concert();
    void add_Type();
    std::string getTypeOfConcert();
    std::string getTypeOfEvent();
};

#endif /* Concert_hpp */
