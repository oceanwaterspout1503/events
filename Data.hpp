//
//  Data.hpp
//  лаба1
//
//  Created by Maria Khaleta on 14.12.17.
//  Copyright © 2017 Maria Khaleta. All rights reserved.
//

#ifndef Data_hpp
#define Data_hpp
#include "Events.hpp"
#include <stdio.h>
class Data{
private:
    int day, // день
    month, // месяц
    year;// год
    int hour,
    minute;
public:
    Data();
    Data(int date_year, int date_month, int date_day, int date_hour, int date_minute);//конструтор принимает данные
    
    ~Data();//деструктор
    void getDate();
    void setTime() ;
    bool checkTime();
};
#endif /* Data_hpp */
