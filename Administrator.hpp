//
//  Administrator.hpp
//  лаба1
//
//  Created by Maria Khaleta on 14.12.17.
//  Copyright © 2017 Maria Khaleta. All rights reserved.
//

#ifndef Administrator_hpp
#define Administrator_hpp
#include "Events.hpp"
#include "Festival.hpp"
#include "Concert.hpp"
#include "Theater.hpp"
#include "User.hpp"
#include <vector>
#include <stdio.h>
class Administrator{
    private:
     vector<Concert> concert;
     vector<Festival> festival;
     vector<Theater> theater;
public:
    void  add_event(vector<Concert> &concert, vector<Festival> & festival,vector<Theater> & theater);
    void  start(vector<Concert> &concert, vector<Festival> & festival, vector<Theater> & theater);
    void  show_event(vector<Concert> &concert, vector<Festival> & festival, vector<Theater> & theater);
};
#endif /* Administrator_hpp */
