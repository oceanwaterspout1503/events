//
//  Place.hpp
//  лаба1
//
//  Created by Maria Khaleta on 14.12.17.
//  Copyright © 2017 Maria Khaleta. All rights reserved.
//

#ifndef Place_hpp
#define Place_hpp
#include "Events.hpp"
#include <stdio.h>
#include <iostream>
#include <string>
using namespace std;
class Place{
private:
    string city,
    adress;
public:
    Place(string place_city, string place_address) : city(place_city), adress(place_address) {
    };
    Place();
    ~Place();
    void setPlace();
    void getPlace(); // отобразить текущий город

};
#endif /* Place_hpp */
