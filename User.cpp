//
//  User.cpp
//  лаба1
//
//  Created by Maria Khaleta on 14.12.17.
//  Copyright © 2017 Maria Khaleta. All rights reserved.
//

#include "User.hpp"

#include <iostream>
#include <vector>

using namespace std;

void User::show_event(vector<Concert> concert, vector<Festival> festival, vector<Theater> theater) {
    for (int i=0; i<concert.size(); i++) {
        cout <<"Событие №"<< i+1 << " " << concert[i].getTypeOfEvent() << " " << concert[i].getTypeOfConcert() << " " << endl;
    }
    for (int i=0; i<festival.size(); i++) {
        cout <<"Событие №"<< i+1 << " " << festival[i].getTypeOfEvent() << " " << festival[i].getTypeOfFestival() << " " << endl;
    }
    for (int i=0; i<theater.size(); i++) {
        cout <<"Событие №"<< i+1 << " " << theater[i].getTypeOfEvent() << " " << theater[i].getTypeOfTheater() << " " << endl;
    }
};

void User :: toReservate(vector<Concert> & concert, vector<Festival> & festival, vector<Theater> & theater) {
    int marker;
    int n; //номер в списке события
    cout << "Выберете тип события для которого хотите забронировать место"<< endl;
    cout << "1 - концерт, 2 - фестиваль, 3 - театральная постановка" << endl;
    cin >> marker;
    switch (marker) {
        case 1:
            cout << "Введите номер концерта - ";
            cin >> n;
            concert[n-1].reserv();
            break;
        case 2:
            cout << "Введите номер концерта - ";
            cin >> n;
            festival[n-1].reserv();
            break;
        case 3:
            cout << "Введите номер концерта - ";
            cin >> n;
            theater[n-1].reserv();
            break;
        default:
            cout << "Проверьте введное значение!";
    }
};

void User :: start(vector<Concert> & concert, vector<Festival> & festival, vector<Theater> & theater) {
    int starter;
    cout << "1 - вывести на екран события, 2 - забронировать место" << endl;
    cin >> starter;
    switch (starter) {
        case 1:
            show_event(concert, festival, theater);
            break;
        case 2:
            toReservate(concert, festival, theater);
            break;
    }
};
