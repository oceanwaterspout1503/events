//
//  Festival.hpp
//  лаба1
//
//  Created by Maria Khaleta on 15.12.17.
//  Copyright © 2017 Maria Khaleta. All rights reserved.
//

#ifndef Festival_hpp
#define Festival_hpp

#include <stdio.h>
#include "Events.hpp"
//#include <iostream>
class Festival : public Events {//наследуется от ивента
private:
    std::string type_of_event = "Festival";
    std::string type_of_festival;
public:
    Festival();
    ~Festival();
    void add_Type();
    std::string getTypeOfFestival();
    std::string getTypeOfEvent();
};

#endif /* Festival_hpp */
