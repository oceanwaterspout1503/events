//
//  User.hpp
//  лаба1
//
//  Created by Maria Khaleta on 14.12.17.
//  Copyright © 2017 Maria Khaleta. All rights reserved.
//

#ifndef User_hpp
#define User_hpp

#include <stdio.h>
#include "Events.hpp"
#include "Administrator.hpp"
#include <iostream>
#include <vector>
using namespace std;
class User{
    private:
     vector<Concert> concert;
     vector<Festival> festival;
     vector<Theater> theater;
public:
void show_event();
void toReservate();
void start();
};
#endif /* User_hpp */
