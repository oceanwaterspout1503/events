//
//  Theater.hpp
//  лаба1
//
//  Created by Maria Khaleta on 14.12.17.
//  Copyright © 2017 Maria Khaleta. All rights reserved.
//

#ifndef Theater_hpp
#define Theater_hpp
#include "Events.hpp"
#include <stdio.h>
#include <iostream>
#include <vector>
class Theater : public Events {//наследуется от ивента
private:
    std::string type_of_event = "Theater";
    std::string type_of_theater;
    public:
    Theater();
    ~Theater();
    void add_Type();
    std::string getTypeOfTheater();
    std::string getTypeOfEvent();
};
#endif /* Theater_hpp */
