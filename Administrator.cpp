//
//  Administrator.cpp
//  лаба1
//
//  Created by Maria Khaleta on 14.12.17.
//  Copyright © 2017 Maria Khaleta. All rights reserved.
//
#include "Administrator.hpp"
#include <vector>
#include <iostream>

using namespace std;
void Administrator::add_event(vector<Concert> &concert, vector<Festival> & festival, vector<Theater> & theater) {
    
    int type;
    bool indicator = true;
    while (indicator) {
        cout << "Выберете тип события: 1 - концерт, 2 - фестиваль, 3 - театральное событие" << endl;
        cin >> type;
        if (type == 1)
            concert.push_back(Concert());
        else if (type == 2)
            festival.push_back(Festival());
        else if (type == 3)
            theater.push_back(Theater());
        else cout << "Введены не 1, 2, 3" << endl;
        cout << "Если хотите выйти из режима добавления событий нажмите 0, иначе 1" << endl;
        cin >> type;
        if (type == 0) indicator = false;
    }
};

void Administrator::start(vector<Concert> & concert, vector<Festival> & festival, vector<Theater> & theater) {
    int variant;
    bool indicator = true;
    while (indicator) {
        cout << "Выберете что делать: 1 - добавить события, 2 - вывести на экран события, 3 - выйти из администратора" << endl;
        cin >> variant;
        switch (variant) {
            case 1:
                add_event(concert, festival, theater);
                break;
            case 2:
                show_event(concert, festival, theater);
                break;
            case 3:
                indicator = false;
                break;
            default:
                cout << "Проверьте введное значение!";
        }
    }
};

void Administrator :: show_event(vector<Concert> concert, vector<Festival> festival, vector<Theater> theater) {
    for (int i=0; i<concert.size(); i++) {
        cout <<"Событие №"<< i+1 << " " << concert[i].getTypeOfEvent() << " " << concert[i].getTypeOfConcert() << " " << endl;
    }
    for (int i=0; i<festival.size(); i++) {
        cout <<"Событие №"<< i+1 << " " << festival[i].getTypeOfEvent() << " " << festival[i].getTypeOfFestival() << " " << endl;
    }
    for (int i=0; i<theater.size(); i++) {
        cout <<"Событие №"<< i+1 << " " << theater[i].getTypeOfEvent() << " " << theater[i].getTypeOfTheater() << " " << endl;
    }
};
